#!/bin/bash

# Download the first 100 bookcovers from the libgen-fiction collection
# Modify or delete "AND id < 100" to get more records,
# or change to "AND id > 1000000" to start at a last known point (ie 1M)

#mkdir -p ../bookshelf/public
#cd ../bookshelf/public
mkdir -p ./bookcovers/fictioncovers/libgen.rs/fictioncovers
mkdir -p ./bookcovers/fictioncovers/libgen.lc/fictioncovers
mkdir -p ./bookcovers/fictioncovers/libgen.li/fictioncovers

echo "CONSOLODATING COVERS SCRAPED FROM WEBSITES (from last run)..."
rsync -a -v --stats --progress --remove-source-files ./bookcovers/fictioncovers/libgen.rs/fictioncovers/ ./bookcovers/fictioncovers/
rsync -a -v --stats --progress --remove-source-files ./bookcovers/fictioncovers/libgen.lc/fictioncovers/ ./bookcovers/fictioncovers/
rsync -a -v --stats --progress --remove-source-files ./bookcovers/fictioncovers/libgen.li/fictioncovers/ ./bookcovers/fictioncovers/

echo "DOWNLOADING COVERS THAT ARE MISSING FROM LOCAL DISK..."

echo "executing sql query..."
sudo mysql -e "SELECT ID, MD5, coverurl FROM libgen_fiction.fiction WHERE coverurl != '' AND id <10 ORDER BY id" | while read ID MD5 coverurl; do
  filename=${MD5^^}
  folder=${MD5:0:2}
  folder=${folder^^}
  subfolder=${MD5:2:2}
  subfolder=${subfolder^^}

if [ $ID != "ID" ]; then # Ignore first query result if variable name was returned instead of data
echo "ID: $ID, MD5: $MD5, Coverurl: $coverurl"

  # Check if bookcover already exists on local disk
  if [[ ! -f "./bookcovers/fictionMD5old/$folder/$subfolder/$filename" && ! -f "./bookcovers/fictioncovers/$coverurl" ]]; then
    echo "=============================================================="
    echo "MISSING: (ID:$ID) coverurl: $coverurl"
    if [[  $coverurl == "http"* || $coverurl == "img."* ]]; then
      echo "  1. Trying STANDARD http(s):// url"
      wget -P ./bookcovers/fictioncovers/ --tries 2 --timeout 5 -c --no-check-certificate --force-directories $coverurl
    elif [[  $coverurl == "/images/I/"* ]]; then
      echo "  1. Trying AMAZON url"
      $wget -P ./bookcovers/fictioncovers/ --tries 2 --timeout 5 -c --no-check-certificate --force-directories "http://ecx.images-amazon.com"$coverurl
    else
      echo "  1. Trying PRIMARY .rs mirror (truncated LG url)..."
      if ! wget -P ./bookcovers/fictioncovers/ --tries 2 --timeout 5 -c --no-check-certificate --force-directories "http://libgen.rs/fictioncovers/"$coverurl; then
        echo "  2. Trying SECONDARY .lc mirror (truncated LG url)"
        if ! wget -P ./bookcovers/fictioncovers/ -c --force-directories "https://libgen.lc/fictioncovers/"$coverurl; then
          echo "3. Trying TERTIARY .li mirror (truncated LG url)"
            if ! wget -P ./bookcovers/fictioncovers/ -c --force-directories "https://libgen.li/fictioncovers/"$coverurl; then
              echo "4. LAST EFFORT (Try as general web url)"
              wget -P ./bookcovers/fictioncovers/ --tries 2 --timeout 5 -c --no-check-certificate --force-directories $coverurl
          fi
        fi
      fi
    fi
  fi
fi

done

echo "CONSOLODATING COVERS SCRAPED FROM WEBSITES (from this run)..."
rsync -a -v --stats --progress --remove-source-files ./bookcovers/fictioncovers/libgen.rs/fictioncovers/ ./bookcovers/fictioncovers/
rsync -a -v --stats --progress --remove-source-files ./bookcovers/fictioncovers/libgen.lc/fictioncovers/ ./bookcovers/fictioncovers/
rsync -a -v --stats --progress --remove-source-files ./bookcovers/fictioncovers/libgen.li/fictioncovers/ ./bookcovers/fictioncovers/
rm -r ./bookcovers/fictioncovers/libgen.rs
rm -r ./bookcovers/fictioncovers/libgen.lc
rm -r ./bookcovers/fictioncovers/libgen.li

echo "BUILDING QUERY (for conversion/renaming/hashing/SQL)"
echo "ID, BookMD5, CoverMD5, CoverURL, SHA256, TimeModified, Width, Height, FileSize" > ./bookcovers-fiction.csv
sudo mysql -e "SELECT ID, MD5, coverurl, TimeLastModified FROM libgen_fiction.fiction WHERE coverurl != '' AND id > 1970158 ORDER BY id" | while read ID MD5 coverurl TimeLastModified; do
  filename=${MD5^^}
  folder=${MD5:0:2}
  folder=${folder^^}
  subfolder=${MD5:2:2}
  subfolder=${subfolder^^}
  extension=${coverurl:(-3)}

  # sudo apt-get install imagemagick
  # Convert all images into jpeg's, and strip file extension
  if [[ -f ./bookcovers/fictioncovers/$coverurl ]]; then
    mkdir -p ./bookcovers/fictionMD5old/$folder/$subfolder
    convert ./bookcovers/fictioncovers/$coverurl ./bookcovers/fictionMD5old/$folder/$subfolder/$filename.jpg
    rm ./bookcovers/fictioncovers/$coverurl
    mv ./bookcovers/fictionMD5old/$folder/$subfolder/$filename.jpg ./bookcovers/fictionMD5old/$folder/$subfolder/$filename
  fi

  # Rename (again), and collect data for log to DQL
  if [[ -f ./bookcovers/fictionMD5old/$folder/$subfolder/$filename ]]; then
    # Generate MD5 hash from bookcover
    newMD5=($(md5sum ./bookcovers/fictionMD5old/$folder/$subfolder/$filename))
    newMD5=${newMD5^^}
    newSHA256=($(sha256sum ./bookcovers/fictionMD5old/$folder/$subfolder/$filename))
    newSHA256=${newSHA256^^}

    newfilename=${newMD5^^}
    newfolder=${newMD5:0:2}
    newfolder=${newfolder^^}
    newsubfolder=${newMD5:2:2}
    newsubfolder=${newsubfolder^^}
    newfilesize=$(stat --format=%s "./bookcovers/fictionMD5old/$folder/$subfolder/$filename")

    # Rename bookcover to its own MD5 (from LG book MD5)
    sudo mkdir -p ./bookcovers/fiction/$newfolder/$newsubfolder
    sudo cp ./bookcovers/fictionMD5old/$folder/$subfolder/$filename ./bookcovers/fiction/$newfolder/$newsubfolder/$newfilename

    # Get the cover dimensions
    read -r file width height <<< $( convert ./bookcovers/fiction/$newfolder/$newsubfolder/$newfilename -format "%f %w %h" info:)
    width=$width
    height=$height

    #echo "log data out to file for later conversion to SQL"
    echo "$ID, $MD5, $newMD5, $newfolder/$newsubfolder/$newfilename, $newSHA256, $TimeLastModified, $width, $height, $newfilesize" >> ./bookcovers-fiction.csv
    echo "$ID, $MD5, $newMD5 $TimeLastModified"
  fi

done

echo "Done!"
