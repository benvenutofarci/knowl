#!/bin/bash
##############################################################
## Knowl Bookshelf - INGEST articles                        ##
## Filename: ./legacy/2-maria-to-json-articles.sh           ##
## https://gitlab.com/lucidhack/knowl                       ##
## https://creativecommons.org/licenses/by/4.0/             ##
## Author(s): lucidhack                                     ##
##############################################################

# PURGE EXISTING FILES
cd /var/tmp
sudo rm -f ~/knowl/ingest/knowl-libgen-articles.json
sudo rm -f /var/tmp/knowl-libgen-articles*.json

# GENERATE MANY JSON FILES (100K at a time)
counter=0
while [ $counter -le 80000000 ]
do
echo $counter
sudo mysql <<ENDSQL
USE libgen_scimag;
DROP TABLE IF EXISTS knowl;
SET connect_work_size = 4000000000;
create table knowl (Description char(255)) engine=CONNECT table_type=JSON file_name='/var/tmp/knowl-libgen-articles-$counter.json' option_list='Pretty=1,Jmode=0,Base=1' lrecl=128 as
SELECT scimag.id "ID",
       scimag.doi "DOI",
       scimag.Title,
       scimag.Author,
       scimag.Year,
       scimag.Month,
       scimag.Day,
       scimag.Volume,
       scimag.Issue,
       scimag.First_Page,
       scimag.Last_Page,
       scimag.Journal,
       scimag.ISBN,
       scimag.ISSNP "ISSNp",
       scimag.ISSNE "ISSNe",
       scimag.MD5,
       scimag.Filesize,
       scimag.JOURNALID "JournalID",
       scimag.AbstractURL,
       magazines.Magazine,
       magazines.Abbr "Abbreviation",
       magazines.Description,
       magazines.Publisher,
       magazines.CATEGORY "Topic"
FROM libgen_scimag.scimag LEFT JOIN libgen_scimag.magazines ON scimag.issne=magazines.issne LIMIT $counter, 100000;
DROP TABLE IF EXISTS knowl;
ENDSQL
counter=$(( $counter + 100000 ))
done

# COMBINE ALL OF THE 1K JSON FILES
sudo cat knowl-libgen-articles*.json >> ~/knowl/ingest/knowl-libgen-articles.json
sudo rm -f knowl-libgen-articles*.json

# DELETE LINES CONTAINING ONLY "[" or "]",
# AND DELETE LEADING WHITESPACE FROM ALL LINES
cd ~/knowl/ingest
sudo sed -i '/^\[/d;/^\]/d;s/^[ \t]*//' knowl-libgen-articles.json

# Next, you should call the index json script
# sudo sh ../bare-metal/index-articles-json.sh
