#!/bin/bash
##############################################################
## Knowl Bookshelf - INGEST Articles                        ##
## Filename: ./legacy/1-maria-ingest-articles.sh            ##
## https://gitlab.com/lucidhack/knowl                       ##
## https://creativecommons.org/licenses/by/4.0/             ##
## Author(s): lucidhack                                     ##
##############################################################

# Change to our ingest folder, and purge last/old file
cd ~/knowl/ingest/
rm -f scimag.sql.gz

# Fetch the articles database from your favorite mirror
wget -c http://libgen.rs/dbdumps/scimag.sql.gz

# Unpack the database
gzip -d -v scimag.sql.gz

# Remove NO_AUTO_CREATE_USER
sed -i 's/,NO_AUTO_CREATE_USER//' scimag.sql

# Recreate a blank database
sudo mysql -e "DROP DATABASE IF EXISTS libgen_scimag;"
sudo mysql -e "CREATE DATABASE libgen_scimag;"

# Import our databse into MariaDB
sudo mysql libgen_scimag < scimag.sql
rm -f scimag.sql

# Next, you should call the json script
# sudo sh ../legacy/2-maria-to-json-articles.sh
